package wssiac;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.*;


/**
 * Created by lilith on 02/06/17.
 */


@WebService
@SOAPBinding(style = Style.RPC)
public interface Data {

    @WebMethod
    boolean addStudent(Student student);

    @WebMethod
    Subject[] getSubject(Student subject) ;

    @WebMethod
    Student getStudent(int id);

    @WebMethod
    float getMedia(float media);

}
