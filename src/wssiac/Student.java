package wssiac;

/**
 * Created by lilith on 02/06/17.
 */
public class Student {

    private int id;
    private String name;
    private String code;
    private String city;
    private int age;
    private Subject [] subject;
    private Float average;



    public int getId() {return id; }

    public void setId(int id) { this.id = id; }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() { return code; }

    public void setCode(String code) { this.code = code; }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Subject[] getSubject() {
        return subject;
    }

    public void setSubject(Subject[] subject) { this.subject = subject; }

    //    public Float getAverage() {
//
//        wssiac.Subject subs = new wssiac.Subject();
//        wssiac.Subject[] discipline;
//
//
//        for (int i = 0; i < subs.getScore() ; i++) {
//
//            discipline = new wssiac.Subject[i];
//
//        }
//
//        average =  discipline / discipline.length;
//
//        return average;
//    }

}
