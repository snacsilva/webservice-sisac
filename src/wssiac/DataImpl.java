package wssiac;

import javax.jws.WebService;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by lilith on 02/06/17.
 */

@WebService (endpointInterface = "wssiac.Data")
public class DataImpl implements Data{

    private static Map<Integer,Student> students = new HashMap<Integer,Student>();

    @Override
    public boolean addStudent(Student student) {
            if(students.get(student.getId()) != null) return false;
            students.put(student.getId(), student);

            return true;
        }

    @Override
    public Student getStudent(int id) {
        return students.get(id);
    }


    @Override
    public Subject[] getSubject(Student subject) {
       return subject.getSubject();
    }

    @Override
    public float getMedia(float media) {
        return 0;
    }
}
